import { Component } from 'react';

export default class ProductCard extends Component {
	constructor(props){
		super(props);
		this.state = {
			value: ''
		}
	}


	onChangeInput = (e) => {
		this.setState({
			value: e.target.value
		})
		console.log(this.state.value);
	}

	onClickHandler = () => {
		this.props.onQuantityAdd(this.state.value)
		this.setState({
			value: ''
		})
	}

	render () {
		return (
			<div className='product-card'>
				<h4>{this.props.data.name}</h4>
				<img src={this.props.data.img_url}/>
				<p>{this.props.data.price}</p>

				<form>
					<input type='number' 
					value={this.state.value} 
					onChange={this.onChangeInput}
					/>
					<button type='button' onClick={this.onClickHandler}>Add to Cart</button>
				</form>
			</div>
			
		)
		
	}
}