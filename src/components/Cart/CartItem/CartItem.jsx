import { Component } from 'react';



export default class CartItem extends Component {
	constructor(props){
		super(props);
	}


	render () {
		return (
			<div className='cart-item'>
				<p>{this.props.item.name}</p>
				<p>{this.props.item.quantity}</p>
			</div>
			
		)
		
	}
}