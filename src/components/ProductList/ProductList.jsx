import ProductCard from '../ProductCard/ProductCard'


function ProductList (props) {
	console.log(props);
	return (
		<>
			{props.data.map(product => {
				return <ProductCard 
				data={product} 
				key={product.id} 
				onQuantityAdd={(quantity) => {
					props.onProductAdd(quantity, product.id)
				}
				}/>
			})}
		</>
	)
}

export default ProductList;